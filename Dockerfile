# Adapted from ijava-binder's Dockerfile:
#   https://github.com/SpencerPark/ijava-binder/blob/master/Dockerfile

FROM debian:bullseye-slim

# Workarounds for debian slim
RUN mkdir -p '/usr/share/man/man1/'
RUN mkdir -p '/etc/ssl/certs/java/'

RUN apt-get update
# Default python3 version in bullseye is 3.9
RUN apt-get install -y openjdk-11-jdk python3-pip git maven

USER root

RUN pip3 install --no-cache-dir jupyter==1.0.0 jupyterlab==3.1.4

# Download Kernel
RUN mkdir ikawa \
  && cd ikawa \
  && git init \
  && git remote add origin "https://gitlab.com/spellcard199/ikawa.git" \
  && git pull --depth=1 origin "8a704754876141058a5cd8802ba9d301a14891d9"

# Package kernel
RUN cd ikawa && mvn package
RUN cd ikawa && python3 install.py --sys-prefix

# Set up the user environment

ENV NB_USER jovyan
ENV NB_UID 1000
ENV HOME /home/$NB_USER

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid $NB_UID \
    $NB_USER

COPY . $HOME
RUN chown -R $NB_UID $HOME

USER $NB_USER

# Launch the notebook server
WORKDIR $HOME
CMD ["jupyter", "notebook", "--ip", "0.0.0.0"]