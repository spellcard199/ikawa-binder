# ikawa-binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/spellcard199%2Fikawa-binder/06a90273617f5fe2528d4efaa18eb74590ee2eab)

(click on the link above to open a notebook)

A [Binder](https://mybinder.org/) compatible repository to try out the [IKawa](https://gitlab.com/spellcard199/ikawa) kernel with just a web browser. Depending on if the docker image is already built, spawning the notebook may take some time and so please be patient with it.

Note: I just limited myself to copying and adapting what SpencerPark did with [IJava Binder](https://github.com/SpencerPark/ijava-binder) (even this README).
